Tesseract OCR service
=====================

Currently has only 2 methods:
    
    `getPulse()`
        - GET http://127.0.0.1:9002/ocr/v1/pulse
        - For heartbeat check

    `readImage(file, json_data)`
        - POST http://127.0.0.1:9002/ocr/v1/read
        - To upload image to the OCR scanner.


To run:

    - Go to application root directory
    - $ mvn clean compile exec:java &

To POST receipt image with receipt info:

    `curl -X POST -H 'Content-Type: multipart/form-data' -H 'Accept: application/json' -i -F "file=@/Users/ronalddennison/IdeaProjects/RooWork/resources/images/patoo.1.jpeg" -F 'json={"shopName":"raggddann", "shopId":1}' http://127.0.0.1:9002/ocr/v1/read`

Repeat multiple times and see that, although the service responds immediately, the actual image processing may take longer and is thus runs executed from a seperate thread.