package com.denkarr.patoo.support.runner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.denkarr.patoo.support.reference.Globals;
import com.denkarr.patoo.support.service.Utils;

public class RunChannel {
	
	/**
     * Allow this route to be run as an application
     */
	public static void main(String[] args) throws Exception {

		// Initialize runtime variables
	    new Globals();
	    
		DefaultCamelContext camelContext = (DefaultCamelContext) Utils.getCamelContext();

		camelContext.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				errorHandler(noErrorHandler());
//				getContext().setTracing(true);

				from(Globals.OCR_REST_ENDPOINT)
//					.setExchangePattern(ExchangePattern.InOut)
					.process(new MappingProcessor(Globals.OCR_SRVC_REST_PROVIDER))
					.marshal().json(JsonLibrary.Jackson);

				/*
				 * Message is sent to queue from within the ocrService
				 */
				from(Globals.OCRSCANNER_IN_QUEUE)
					.setExchangePattern(ExchangePattern.InOut)
					.delay(4000)
					.log("body: ${in.body}")
					.bean(Globals.OCRSCANNER_PROVIDER)
					.marshal().json(JsonLibrary.Jackson)
					.log("out: ${body}")
					;
				
			}  /* END configure */

		});  /* END RouteBuilder */

		Thread.sleep(1000);
		camelContext.start();
		
	} /* END Main */

    // Mapping the request to object's invocation
    private static class MappingProcessor implements Processor {
        
        private Class<?> beanClass;
        private Object instance;
        
        public MappingProcessor(Object obj) {
            beanClass = obj.getClass();
            instance = obj;
        }
         
        public void process(Exchange exchange) throws Exception {
            String operationName = exchange.getIn().getHeader(CxfConstants.OPERATION_NAME, String.class);
            Method method = findMethod(operationName, exchange.getIn().getBody(Object[].class));
            try {
                Object response = method.invoke(instance, exchange.getIn().getBody(Object[].class));
                exchange.getOut().setBody(response);
            }  catch (InvocationTargetException e) {
                throw (Exception)e.getCause();
            }
        }
        
        private Method findMethod(String operationName, Object[] parameters) throws SecurityException, NoSuchMethodException {            
            return beanClass.getMethod(operationName, getParameterTypes(parameters));
        }
        
        private Class<?>[] getParameterTypes(Object[] parameters) {
            if (parameters == null) {
                return new Class[0];
            }
            Class<?>[] answer = new Class[parameters.length];
            int i = 0;
            for (Object object : parameters) {
                answer[i] = object.getClass();
                i++;
            }
            return answer;
        }
    }

}

/* END Runner */
