package com.denkarr.patoo.support.model;

public class ReceiptItem {
	private String name;
	private double price;
	private boolean isTaxed;
	
	public ReceiptItem() {
	}
	
	public ReceiptItem(String name, double price, boolean isTaxed) {
		this();
		 setName(name);
		 setPrice(price);
		 setTaxed(isTaxed);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean isTaxed() {
		return isTaxed;
	}
	public void setTaxed(boolean isTaxed) {
		this.isTaxed = isTaxed;
	}
}
