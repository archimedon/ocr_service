package com.denkarr.patoo.support.model;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


// @javax.xml.bind.annotation.XmlRootElement(name = "ReceiptImage")
public class ReceiptImage implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 96148612979052773L;
	private Date created;
	private java.math.BigInteger shopId;
	private String shopName;
	private URL imageUrl;
	private URL textURL;
	private Collection<ReceiptItem> items = new HashSet<ReceiptItem>();
	
	public ReceiptImage() { }

	public ReceiptImage(Date date, String string, URL url) {
		this.created = date;
		this.shopName = string;
		this.imageUrl = url;
	}
	
	public Date getCreated() {
		return created;
	}
	
	public void setCreated(Date created) {
		this.created = created;
	}
	
	public java.math.BigInteger getShopId() {
		return shopId;
	}
	
	public void setShopId(java.math.BigInteger shopId) {
		this.shopId = shopId;
	}
	
	public String getShopName() {
		return shopName;
	}
	
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	public URL getImageUrl() {
		return imageUrl;
	}
	
	public void setImageUrl(URL imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public URL getTextURL() {
		return textURL;
	}
	
	public void setTextURL(URL textURL) {
		this.textURL = textURL;
	}
	
	public Collection getItems() {
		return items;
	}

	private void setItems(Collection items) {
		this.items = items;
	}

	public void addItem(ReceiptItem item) {
		this.items.add( item);
	}
	
	public String toString(){
		// TODO Replace with toString or Property -Builder.
		String[] params = new String[4];
		int i=0;
		for (String str : "shopName shopId textURL imageUrl".split("\\s+")) {
			try {
				Field field = getClass().getDeclaredField(str);
				params[i++] = str + ": " + field.get(this);
			} catch (Exception e) { }
		}
		
		return String.join("\n", params);
	}
}
