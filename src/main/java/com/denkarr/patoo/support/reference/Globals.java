package com.denkarr.patoo.support.reference;

import com.denkarr.patoo.support.service.ImageScanner;
import com.denkarr.patoo.support.service.TesseractOCRService;

/**
 * @author ronalddennison
 */
public class Globals {

    public static final String ACCESSIBLE_HOST = "roots.rock.jm";

    public static final TesseractOCRService OCR_SRVC_REST_PROVIDER = new TesseractOCRService();
    
    public static final String OCR_REST_ENDPOINT = "cxfrs://http://localhost:{{restEndpointPort}}"
	        + "?resourceClasses=" + OCR_SRVC_REST_PROVIDER.getClass().getName();
	
    public static final String OCR_SRVC_PULSE_METHOD = "getPulse";
    public static final String OCR_SRVC_UPLOAD_METHOD = "uploadImage";
    public static final String OCR_SRVC_OUTPUT_DIR = "/tmp";

    /**
     * Setup scanner
     */
    public static final ImageScanner OCRSCANNER_PROVIDER = new ImageScanner();

    public static final String  OCRSCANNER_REST_ENDPOINT = "cxfrs://http://localhost:{{restEndpointPort}}"
	        + "?resourceClasses=" + OCRSCANNER_PROVIDER.getClass().getName();
    
    public static final String OCRSCANNER_IN_QUEUE = "activemq:queue:in";

	public static final String OCRSCANNER_OUTPUT_DIR = "/tmp";

	public static final String OCRSCANNER_PULSE_METHOD = "getPulse";
	
	public static final String OCRSCANNER_PARSE_METHOD = "scanImage";
	
	/**
	 * The webservice output is input to the image-processor
	 */
	public static final String OCR_SRVC_OUT_QUEUE = OCRSCANNER_IN_QUEUE;
	/**
	 * Scanner-input is rest-service output.
	 */
	public static final String OCRSCANNER_INPUT_DIR = OCR_SRVC_OUTPUT_DIR;
	
	
    
//    public static final String OCRSCANNER_MSG_QUEUE = "activemq:scanner:in";


	/**
	 * 
	 */
	public Globals() {
		System.setProperty("soapEndpointPort", "9006");
		System.setProperty("restEndpointPort", "9002");
	}

}

