package com.denkarr.patoo.support.reference;

import java.util.Map;

public class BeanConfig {

	public Object provider;
	public String endpoint;
	public String queueName;
	public Map<String, String> methodConf;
	

	public BeanConfig() {
		
	}

		
}

/*
public static final TesseractOCRService OCR_SRVC_REST_PROVIDER = new TesseractOCRService();
public static final ImageScanner OCRSCANNER_PROVIDER = new ImageScanner();

public static final String OCR_REST_ENDPOINT = "cxfrs://http://localhost:{{restEndpointPort}}"
        + "?resourceClasses=" + OCR_SRVC_REST_PROVIDER.getClass().getName();

public static final String ACCESSIBLE_HOST = "roots.rock.jm";
public static final String OCRSCANNER_MSG_QUEUE = "activemq:queue:in";

public static final String OCRSCANNER_INPUT_DIR = "/tmp";
public static final String OCRSCANNER_OUTPUT_DIR = "/tmp";

public static final String OCR_SRVC_PULSE_METHOD = "getPulse";

public static final String OCRSCANNER_PARSE_METHOD = "scanImage";

*/