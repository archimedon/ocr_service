package com.denkarr.patoo.support.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.DefaultCamelContext;

import com.denkarr.patoo.support.reference.Globals;

public final class Utils {
	
	private static DefaultCamelContext camelContext = null;
	
	public static File fcopy(InputStream in , String dest) throws IOException {
		File file = new File(dest);
		OutputStream os = new FileOutputStream(file);
		int total = 0;
		for  (int i =-1; ( i = in.read() ) > -1; os.write(i))	{
			total++;
		}

		os.close();
		in.close();

		System.err.println("Copied " + total + " bytes to " + dest);

		return file;
	}

	public static CamelContext getCamelContext() {
		if (camelContext == null) {
			camelContext=new DefaultCamelContext();
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost?broker.persistent=false");
			camelContext.addComponent("activemq", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
			camelContext.addComponent("properties", new PropertiesComponent());
		}
		return camelContext;
	}

	public static String makeUploadPath(String name) {
		return Globals.OCRSCANNER_INPUT_DIR + File.separatorChar + name;
	}

	public static String makeTextReadPath(String name) {
		return Globals.OCRSCANNER_OUTPUT_DIR + File.separatorChar + name.replaceFirst("\\.\\w+$", ".txt");
	}
	
}
