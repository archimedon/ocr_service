package com.denkarr.patoo.support.service;

/**
 * Attempt to streamline the format of a WS or REST response.
 * 
 * @author ronalddennison
 *
 */
public class DefaultReply implements Reply {

	static protected Reply reply = null;
	protected int status ;
	protected StatusInfo statusInfo = null;
	protected Object outMessage = null;
	
	protected DefaultReply() {
		this.statusInfo = new StatusInfo();
	} 

	protected DefaultReply(Object message, int status) {
		this();
		init(this, message, status); 
	} 
	
	protected static final DefaultReply init(DefaultReply reply, Object message, int status)  {
		reply.setOutMessage(message);
		reply.setStatus(status);
		reply.statusInfo.setStatusCode(status);
		return reply;
	}
	
	@Override
	public int getStatus() {
		return status;
	}

	@Override
	public Object getOutMessage() {
		return outMessage;
	}

	@Override
	public StatusInfo getStatusInfo() {
		return statusInfo;
	}

	@Override
	public Reply setStatus(int status) {
		this.status = status;
		return this;
	}

	@Override
	public Reply setStatusInfo(StatusInfo statusInfo) {
		this.statusInfo = statusInfo;
		return this;
	}

	@Override
	public Reply setOutMessage(Object outMessage) {
		this.outMessage = outMessage;
		return this;
	}

}
