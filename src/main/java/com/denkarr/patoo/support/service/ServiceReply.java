package com.denkarr.patoo.support.service;

/**
 * Add convenience methods to Default Reply.
 * 
 * @author ronalddennison
 */
public class ServiceReply extends DefaultReply {

	/*
	 * Contextually unique ID.
	 */
	protected String cuid = null;
	

	static final ServiceReply getInstance() {
		if (reply == null) {
			reply = new ServiceReply();
		}
		return (ServiceReply) reply;
	}

	static final DefaultReply getInstance(Object message, int status) {
		return init( (DefaultReply) getInstance(), message, status); 
	}

	public String getCuid() {
		return cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	static public ServiceReply ok(Object message) {
		return (ServiceReply) getInstance(message, STATUS_OK);
	}

	public ServiceReply setStatusInfo(int statusCode, String family, String reasonPhrase) {
		this.statusInfo.setStatusCode(statusCode).setFamily(family).setReasonPhrase(reasonPhrase);
		return this;
	}
	
	public ServiceReply setStatusInfo(int statusCode, String family) {
		this.statusInfo.setStatusCode(statusCode).setFamily(family);
		return this;
	}
	
	public ServiceReply setStatusFamily(String family) {
		this.statusInfo.setFamily(family);
		return this;
	}
	
	public ServiceReply setStatusReason(String reasonPhrase) {
		this.statusInfo.setReasonPhrase(reasonPhrase);;
		return this;
	}
}
