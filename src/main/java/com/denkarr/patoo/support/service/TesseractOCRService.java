/**
 * 
 */
package com.denkarr.patoo.support.service;

import java.net.URL;
import java.util.Date;
import java.util.concurrent.Future;

import javax.activation.DataHandler;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.support.SynchronizationAdapter;
import org.springframework.beans.BeanUtils;

import com.denkarr.patoo.support.model.ReceiptImage;
import com.denkarr.patoo.support.reference.Globals;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author ronalddennison
 */
public class TesseractOCRService implements OCRService {

	public TesseractOCRService() {
		
	}

	@Override
	public Reply getPulse() {
		try {
			Thread.sleep(4000);
			System.err.println("Felizedades");

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ServiceReply reply =  (ServiceReply) ServiceReply.ok("bishop");
		reply
			.setStatusInfo(ServiceReply.STATUS_OK, "SUCCESSFUL", "SUCCESS");
		return reply;
	}


	/* (non-Javadoc)
	 * @see com.denkarr.patoo.support.service.OCRService#readImage(DataHandler file, String name)
	 */
	@Override
	public ServiceReply uploadImage(DataHandler dh, String json) {
		ReceiptImage payload = null;
		try {

			// Copy the user data to the Image object.
			payload = new ObjectMapper().readValue(json, ReceiptImage.class);
			
			String img_file = Utils.makeUploadPath(dh.getName());
			String text_file = Utils.makeTextReadPath(dh.getName());
						
			// Doesn't work without first being reset
			dh.getInputStream().reset();
			
			Utils.fcopy(dh.getInputStream(), img_file);
			
			payload.setImageUrl(new URL("http", Globals.ACCESSIBLE_HOST,  img_file));
			payload.setTextURL( new URL("http", Globals.ACCESSIBLE_HOST,  text_file));
			payload.setCreated(new Date());

			CamelContext camelContext = Utils.getCamelContext();
			ProducerTemplate template = camelContext.createProducerTemplate();
			final ReceiptImage target = new ReceiptImage();
			BeanUtils.copyProperties(payload, target);
			Exchange exchange = camelContext.getEndpoint(Globals.OCR_SRVC_OUT_QUEUE).createExchange();
			exchange.getIn().setBody(payload);
			Future<Exchange> future = template.asyncCallback(Globals.OCRSCANNER_IN_QUEUE, exchange, new SynchronizationAdapter() {
			    @Override
			    public void onComplete(Exchange exchange) {
			    	System.err.println("received 1: " + exchange.getIn().getBody());
			    	System.err.println("received out 1: " + exchange.getOut().getBody());
			    }
			});
//	    	System.err.println("received out 1: " + future.get().getOut().getBody());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ServiceReply
			.ok(payload)
			.setStatusInfo(200, "SUCCESS", "SUCCESS");
	}
}
