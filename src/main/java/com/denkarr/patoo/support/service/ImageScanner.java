/**
 * 
 */
package com.denkarr.patoo.support.service;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;

import com.denkarr.patoo.support.model.ReceiptImage;
import com.denkarr.patoo.support.model.ReceiptItem;

/**
 * @author ronalddennison
 */
public class ImageScanner {

	public ImageScanner() {
		// TODO Auto-generated constructor stub
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	public Reply scanImage(@Body ReceiptImage rimg) {
		
		String  path = rimg.getImageUrl().toString();
		
		rimg.addItem( new ReceiptItem("Limes local per dozen", 224.99, false) );
		rimg.addItem( new ReceiptItem("Eggs half Dozen", 180.60, true) );
		rimg.addItem( new ReceiptItem("SUGAR GRANULTED PER kg", 222.00, true) );
		
		return ServiceReply.ok(rimg);
	}

	/*
	 * 
	 // Alternative  - use whole exchange.

	@Produces(MediaType.APPLICATION_JSON)
	public void scanImage(Exchange exchange) {
		ReceiptImage rimg = exchange.getIn().getBody(ReceiptImage.class);
		String  path = rimg.getImageUrl().toString();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("PATH: " + path);
		exchange.getOut().setBody( ServiceReply.ok("{\"key\":\"" + path + "\"}"));
	}
	
*/	
}
