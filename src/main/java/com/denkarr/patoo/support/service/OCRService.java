package com.denkarr.patoo.support.service;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.ws.rs.Consumes;
import javax.ws.rs.Encoded;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

/**
 * OCRService is the frontend of the image processor.
 * 
 * @author ronalddennison
 */
@Path("/ocr/v1")
@Produces(MediaType.APPLICATION_JSON)
public interface OCRService {
	

	/**
	 * Takes the uploaded <code>file</code> and forwards it to the ImageProcessor.
	 * 
	 * Expect:
	 * 		- the uploaded file to be an image.
	 * 		- the JSON should contain:
	 * 			shopName, shopId
	 * 
	 *		Eg.
	 * 			String json = "{\"shopName\" : \"macy's\" ,\"shopId\" : 1001}"
	 * 
	 * Returns a ReceiptImage object wrapped in a Reply object.
	 * 
	 * @param file - the image of the receipt.
	 * @param json - JSON formatted additional receipt data.
	 * @return Confirmation of receipt-image with URL to extracted text.
	 */
	@WebMethod
	@POST
	@Path("/read")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
  	public ServiceReply uploadImage( @Multipart(value = "file")  DataHandler file, @Encoded @FormParam("json")  String json );


	/**
	 * @return 
	 */
	@WebMethod
	@GET
	@Path("/pulse")
	@Produces(MediaType.APPLICATION_JSON)
	public Reply getPulse();

}
