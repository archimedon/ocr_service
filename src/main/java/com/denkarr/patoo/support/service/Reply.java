package com.denkarr.patoo.support.service;

import javax.ws.rs.core.Response.Status;

public interface Reply {

	static final int STATUS_OK = Status.OK.getStatusCode();

	public int getStatus();
	
	public Object getOutMessage();
	
	public StatusInfo getStatusInfo();

	public Reply setStatus(int status);

	public Reply setStatusInfo(StatusInfo statusInfo);

	public Reply setOutMessage(Object outMessage);
	
}
