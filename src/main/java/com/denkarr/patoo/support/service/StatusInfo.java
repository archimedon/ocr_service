package com.denkarr.patoo.support.service;

public class StatusInfo {
	
	private int statusCode;
	private String reasonPhrase;
	private String family;
	
	public StatusInfo() {
	}
	
	public StatusInfo(int status) {
		this.setStatusCode(status);
	}

	public int getStatusCode() {
		return statusCode;
	}
	
	public StatusInfo setStatusCode(int statusCode) {
		this.statusCode = statusCode;
		return this;
	}
	
	public String getReasonPhrase() {
		return reasonPhrase;
	}
	
	public StatusInfo setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
		return this;
	}
	
	public String getFamily() {
		return family;
	}
	
	public StatusInfo setFamily(String family) {
		this.family = family;
		return this;
	}

}
