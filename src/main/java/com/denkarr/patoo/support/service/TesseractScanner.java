/**
 * 
 */
package com.denkarr.patoo.support.service;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Exchange;

import com.denkarr.patoo.support.model.ReceiptImage;

/**
 * @author ronalddennison
 *
 */
public class TesseractScanner {

	/**
	 * 
	 */
	public TesseractScanner() {
		// TODO Auto-generated constructor stub
	}

	public Reply scanImage(@Body ReceiptImage rimg) {
		String  path = rimg.getImageUrl().toString();
		System.err.println("PATH: " + path);
		return ServiceReply.ok("{\"key\":\"" + path + "\"}");
	}
}
