
package com.denkarr.patoo.support.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for book complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="book"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://localhost}thechapter" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "book", propOrder = {
    "thechapter",
    "id"
})
public class Book {

    protected Book thechapter;
    @XmlElement(namespace = "")
    protected int id;

    /**
     * Gets the value of the thechapter property.
     * 
     * @return
     *     possible object is
     *     {@link Book }
     *     
     */
    public Book getThechapter() {
        return thechapter;
    }

    /**
     * Sets the value of the thechapter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Book }
     *     
     */
    public void setThechapter(Book value) {
        this.thechapter = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

}
