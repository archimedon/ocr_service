/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.denkarr.patoo.support.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import com.denkarr.patoo.support.model.Book;

@Path("/bookstore/{id}")
public class BookstoreIdResource {

    @GET
    @Produces("application/xml")
    public Book get(@PathParam("id") String id) {
        //TODO: implement
        return null;
    }

}